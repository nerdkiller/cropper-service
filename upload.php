<?php
  error_reporting(E_ALL);
  ini_set("display_errors", "On");

  function randnumb($length = 3) {
    return substr(str_shuffle(str_repeat($x='0123456789', ceil($length/strlen($x)) )),1,$length);
  }

  $ok = true;

  if($_FILES["image"]["name"] == "") {
    echo "No image was provided.";
    $ok = false;
  }

  if($_FILES["image"]["size"] > 9999999) {
    echo "File is too large.";
    $ok = false;
  }

  if($ok) {
    $fname = basename($_FILES["image"]["name"]);
    $ext = pathinfo($fname)["extension"];
    $fname2 = time() . "_" . randnumb() . "." . $ext;
    $target = "uploads/" . $fname2;

    if (move_uploaded_file($_FILES["image"]["tmp_name"], $target)) {
      $all_sides = ["top", "bottom", "left", "right"];
      $sides = [];
      $threshold = 400;
      $border = 0;

      for ($i = 0; $i < count($all_sides); $i++) {
        if(isset($_POST["side_" . $all_sides[$i]])) {
          array_push($sides, $all_sides[$i]);
        }
      }

      if(count($sides) == 0) {
        echo "No sides selected.";
        exit(0);
      }

      $sidestring = implode(",", $sides);

      if(isset($_POST["threshold"])) {
        $threshold = $_POST["threshold"];
      }

      if(isset($_POST["border"])) {
        $border = $_POST["border"];
      }
      
      $date1 = microtime(true);
      $path = exec("bin/cropper " . $sidestring . " " . $threshold . " " . $border . " " . $target);
      $date2 = microtime(true);

      $style = "body, html {
        background-color: white;
        color: black;
        font-family: sans-serif;
        font-size: 18px;
      } 
      
      .item {
        display: inline-block;
        padding: 20px;
      } 
      
      .title {
        padding-bottom: 5px;
        display: block;
      }

      .image {
        border: 8px solid blue;
        max-height: 800px;
        max-width: 800px;
      }
      
      .info {
        margin-left: 20px;
        margin-right: 20px;
        margin-top: 20px;
        margin-bottom: 15px;
        padding: 5px;
        background-color: #dfe4fb;
      }";

      $topheader = "<title>Cropper Result</title>
      <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
      <link rel='shortcut icon' href='guy.jpg?v=1' type='image/x-icon'>";

      $diff = round(($date2 - $date1), 2);
      $seconds = "seconds";

      if($diff == 1) {
        $seconds = "second";
      }

      echo "<head>" . $topheader . "<style>" . $style . "</style></head>";

      $msg = "&nbsp;&nbsp;|&nbsp;&nbsp; The image will be deleted in 2 days";
      $mib = "&nbsp;&nbsp;|&nbsp;&nbsp; Size: " . round(filesize($path) / 1024 / 1024, 1) . " MiB";
      echo "<div class='info'>Crop done in " . $diff . " " . $seconds . $mib . $msg . "</div>";
      echo "<div class='item'><img class='image' src='" . $path . "'></div>";

      unlink($target);
    } else {
      echo "There was an error uploading the image.";
    }    
  }
?>